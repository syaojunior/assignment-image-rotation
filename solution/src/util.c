#include "util.h"
#include <stdio.h>

enum open_status open_file(FILE** file, const char *path, const char *modes) {
    *file = fopen(path, modes);
    if (*file != NULL) return OPEN_OK;
    return OPEN_FAILED;
}

enum close_status close_file(FILE *file) {
    if (file == NULL) {
        return CLOSE_FAILED;
    }
    if (fclose(file)) {
        return CLOSE_FAILED;
    }
    return CLOSE_OK;
}
