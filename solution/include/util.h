#include "status.h"
#include <stdio.h>

enum open_status open_file(FILE **file, const char *path, const char *modes);

enum close_status close_file(FILE *file);
