#ifndef TMP333_STATUS_H
#define TMP333_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_FAILED,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_SOURCE,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum open_status { OPEN_OK = 0, OPEN_FAILED};
enum close_status { CLOSE_OK = 0, CLOSE_FAILED};

#endif //TMP333_STATUS_H
